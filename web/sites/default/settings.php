<?php

/**
 * @file
 */

use \Symfony\Component\HttpFoundation\Request;

/* Core settings */
$settings["config_sync_directory"] = 'sites/default/files/private/config/sync';
$settings['container_yamls'][] = $app_root . '/' . $site_path . '/services.yml';
$settings['entity_update_batch_size'] = 50;
$settings['entity_update_backup'] = TRUE;
$settings['file_private_path'] = 'sites/default/files/private/files';
$settings['file_scan_ignore_directories'] = [
  'node_modules',
  'bower_components',
];
$settings['hash_salt'] = getenv('DRUPAL_HASH_SALT');
/* $settings['trusted_host_patterns'][] = getenv('DRUPAL_HOST'); */
$settings['update_free_access'] = FALSE;
$config_directories['sync'] = '../config/sync';

/* Default database */
$databases['default']['default'] = [
  'driver'   => 'mysql',
  'host'     => getenv('DRUPAL_DB_HOST'),
  'username' => getenv('DRUPAL_DB_USER'),
  'password' => getenv('DRUPAL_DB_PASS'),
  'database' => getenv('DRUPAL_DB_NAME'),
];

/* Reverse proxy */
$settings['reverse_proxy'] = TRUE;
$settings['reverse_proxy_addresses'] = [
  '35.191.0.0/16',
  '130.211.0.0/22',
];
$settings['reverse_proxy_trusted_headers'] = Request::HEADER_X_FORWARDED_FOR |
  Request::HEADER_X_FORWARDED_PROTO | Request::HEADER_X_FORWARDED_PORT;

if(isset($_SERVER['HTTP_X_SUCURI_CLIENTIP']))
{
    $_SERVER["REMOTE_ADDR"] = $_SERVER['HTTP_X_SUCURI_CLIENTIP'];
}

if (file_exists(__DIR__ . '/settings.local.php')) {
  include __DIR__ . '/settings.local.php';
}
