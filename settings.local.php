<?php

unset($settings['trusted_host_patterns']);


$databases['default']['default'] = [
  'database' => 'drupal8',
  'username' => 'drupal8',
  'password' => 'drupal8',
  'host' => 'database',
  'port' => '3306',
  'driver' => 'mysql',
  'prefix' => '',
  'collation' => 'utf8mb4_general_ci',
];

ini_set("memory_limit", "4096M");
$settings['file_public_path'] = 'sites/default/files';
$settings['file_private_path'] = 'sites/default/files/private/files';
$settings['hash_salt'] = 'RKXOBmILaGMGxnclTWwj75SZbfrGPBx3giHUf4vsX08pZp-rJcVX1xyCF4L7_wz5aXps8Q6hGQ';
$settings["file_temp_path"] = "sites/default/files/private/temp";
$config_directories[CONFIG_SYNC_DIRECTORY] = 'sites/default/files/private/config/sync';
