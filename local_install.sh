#!/usr/bin/env bash

site="${PWD##*/}"
siteroot='web/sites/default'
lando rebuild -y
lando jenkins-purge-files
lando jenkins-purge-db
lando jenkins-import-files
lando jenkins-import-db
lando jenkins-purge-files
lando jenkins-purge-db
printf 'Site root is: %s\n' "$siteroot"
cp ./settings.local.php "$siteroot"/settings.local.php
cp ./drushrc.local.php "$siteroot"/drushrc.local.php
if [ "$HOST" = 'cwb-dev-box' || "$HOSTNAME" = 'cwb-dev-box' ]; then
  cp ./drushrc.ngrok.php "$siteroot"/drushrc.ngrok.php
fi
for dir in files files/private files/private/files files/private/sync files/private/temp
do
  printf 'Creating folder: %s\n' "$siteroot/$dir"
  mkdir -p "$siteroot/$dir"
done
# Alleged Composer optimizations
# @see https://stackoverflow.com/questions/28436237/why-is-php-composer-so-slow
lando composer config --global repo.packagist composer https://packagist.org
lando composer clearcache
lando composer install --no-ansi --no-interaction --no-progress --ignore-platform-reqs
lando composer update
tar -xvzf ./files_export.tar.gz -C "$siteroot"
lando rebuild -y
sleep 5
lando db-import site_db.sql.gz
lando drush cr
lando rebuild -y

# -- Uncomment to automatically remove site_db.sql.gz and files_export.tar after site build --
# lando local-clean-assets

lando list --app "$site"
python3 -m webbrowser "$(lando drush uli admin /admin/reports/status)"
